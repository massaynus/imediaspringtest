package de.imedia24.shop

import de.imedia24.shop.domain.product.ProductCreationRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import java.math.BigDecimal

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [ ShopApplication::class ])
class ShopApplicationTests(
	@LocalServerPort
	private var port: Int
) {

	private val baseUrl: String = "http://localhost:$port/products"

	companion object {
		private var counter: Int = 0
	}

	@Autowired
	private lateinit var restTemplate: TestRestTemplate

	fun setUp() {
		val products: List<ProductCreationRequest> = listOf(
			ProductCreationRequest(sku = "${++ShopApplicationTests.counter}", name = "IPhone 13 Pro Max", price = BigDecimal(1300), stockStatus = "InStock"),
			ProductCreationRequest(sku = "${++ShopApplicationTests.counter}", name = "IPhone 13 Pro", price = BigDecimal(1100), stockStatus = "OutOfStock"),
			ProductCreationRequest(sku = "${++ShopApplicationTests.counter}", name = "IPhone 13", price = BigDecimal(900), stockStatus = "InStock"),
		)

		products.map {
			restTemplate.postForEntity(
				baseUrl,
				it,
				ProductResponse::class.java
			)
		}.forEach {
			Assertions.assertNotNull(it, "Got back a not null creation response")
			Assertions.assertEquals(
				HttpStatus.CREATED,
				it.statusCode,
				"The creation for ${it.body?.sku} completed successfully"
			)
		}
	}

	@Test
	fun createProducts() {
		setUp()
	}

	@Test
	fun findBySkusTest() {
		setUp()
		val result = restTemplate.getForEntity(
			"$baseUrl/batch/1,2,3",
			arrayOf<ProductResponse>()::class.java
		)

		Assertions.assertNotNull(result, "Got non null response")
		Assertions.assertEquals(HttpStatus.OK, result?.statusCode, "Got correct status code")
		Assertions.assertTrue(result.body?.size!! > 0, "Got populated data in Body")
	}

	@Test
	fun partialProductUpdate() {
		setUp()
		restTemplate.put(
			"$baseUrl/1",
			ProductUpdateRequest(name = null, description = null, price = BigDecimal(1400), stockStatus = "InStock")
		)

		val result = restTemplate.getForEntity(
			"$baseUrl/1",
			ProductResponse::class.java
		)

		Assertions.assertNotNull(result, "Got non null response")
		Assertions.assertEquals(HttpStatus.OK, result?.statusCode, "Got correct status code")
		Assertions.assertTrue(result.body?.sku!! == "1", "Got correct SKU")
		Assertions.assertTrue(result.body?.name!! == "IPhone 13 Pro Max", "Got correct name")
		Assertions.assertTrue(result.body?.price!! == BigDecimal(1400), "Got correct and updated price")
	}
}
