-- I don't the fact that I have to write SQL for migrations
-- because now these migrations are database dependent.
-- Most modern ORMs have the ability to scaffold migrations from the Entities
-- and run them, while you have to pay for plugins to be able to do the same in Java/Kotlin

ALTER TABLE products
ADD stock_status VARCHAR(125) NOT NULL