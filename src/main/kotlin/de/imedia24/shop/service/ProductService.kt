package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.entity.StockStatus
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductCreationRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import org.springframework.stereotype.Service
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse()
    }

    fun findProductBySkus(skus: List<String>): List<ProductResponse?> {

        // The reason for the two implementations and the preference of the uncommented one would be
        // the possibility of making use of databases ability to get multiple records at once
        // instead of running a query for each SKU using the 'IN' operator/keyword

        // I wanted to do some gymnastics with piping to return a HashMap like {[sku]: product}
        // but it would require to return null values for non found SKUs which I wanted to filter out, which made
        // me ditch the idea
        return productRepository.findAllBySkuIn(skus).map { it.toProductResponse() }


        // return skus.map { findProductBySku(it) }
    }

    fun createProduct(product: ProductCreationRequest): ProductResponse {
        val now = ZonedDateTime.now()

        val productEntity =  ProductEntity(
            sku = product.sku,
            name = product.name,
            description = product.description,
            price = product.price,
            stockStatus = StockStatus.valueOf(product.stockStatus),
            createdAt = now,
            updatedAt = now
        )

        val newProduct = productRepository.save(productEntity)
        return newProduct.toProductResponse()
    }

    fun updateProduct(sku: String, data: ProductUpdateRequest): ProductResponse? {
        val product = productRepository.findBySku(sku)

        return if (product == null) {
            null
        } else {
            val newProduct = ProductEntity(
                sku = product.sku,
                createdAt = product.createdAt,
                updatedAt = ZonedDateTime.now(),
                name = data.name ?: product.name,
                description = data.description ?: product.description,
                price = data.price ?: product.price,
                stockStatus = if (data.stockStatus == null) product.stockStatus else StockStatus.valueOf(data.stockStatus)
            )

            productRepository.save(newProduct).toProductResponse()
        }


    }
}
