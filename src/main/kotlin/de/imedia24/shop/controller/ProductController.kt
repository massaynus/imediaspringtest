package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductCreationRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products/batch/{skus}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(
        @PathVariable("skus") skus: List<String>
    ): ResponseEntity<List<ProductResponse?>> {
        logger.info("Request for products $skus")

        val products = productService.findProductBySkus(skus)

        // What we do here is entirely base on the need, if it is an all or nothing endpoint
        // we can return 404 if one product is null
        // but in most cases for an empty array can be returned if no matching records were found!
        return ResponseEntity.ok(products)
    }


    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun createProduct(
        @RequestBody product: ProductCreationRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request to create product ${product.sku}")

        if (product.sku != "" && productService.findProductBySku(product.sku) != null) {
            logger.info("CONFLICTED on creating existing product $product.sku")
            return ResponseEntity.status(HttpStatus.CONFLICT).build()
        }

        return try {
            val createdProduct = productService.createProduct(product)
            ResponseEntity
                .created(URI("/products/${createdProduct.sku}"))
                .body(createdProduct)
        } catch (e: Exception) {
            logger.error("Request to create product ${product.sku} failed", e)
            ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }

    }


    @PutMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun updateProduct(
        @RequestBody product: ProductUpdateRequest,
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request to update product $sku")

        if (sku == "") {
            logger.info("couldn't update product because of lack of a target SKU")
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build()
        }

        return try {
            val updatedProduct = productService.updateProduct(sku, product)
            if (updatedProduct == null) {
                ResponseEntity.notFound().build()
            } else {
                ResponseEntity.ok(updatedProduct)
            }
        } catch (e: Exception) {
            logger.error("Request to update product $sku failed", e)
            ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }

    }
}
