package de.imedia24.shop.db.entity

import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.*


enum class StockStatus { InStock, OutOfStock }

// I have given these fields default values cuz the ".Save()" kept complaining about the absence of a
// default constructor in the ProductEntity class
@Entity
@Table(name = "products")
data class ProductEntity(
    @Id
    @Column(name = "sku", nullable = false)
    val sku: String = "",

    @Column(name = "name", nullable = false)
    val name: String = "",

    @Column(name = "description")
    val description: String? = null,

    @Column(name = "price", nullable = false)
    val price: BigDecimal = BigDecimal(0),

    @Enumerated(EnumType.STRING)
    @Column(name = "stock_status", nullable = false)
    val stockStatus: StockStatus = StockStatus.InStock,

    @UpdateTimestamp
    @Column(name = "created_at", nullable = false)
    val createdAt: ZonedDateTime = ZonedDateTime.now(),

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    val updatedAt: ZonedDateTime = ZonedDateTime.now()
)