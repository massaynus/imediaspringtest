package de.imedia24.shop.db.repository

import de.imedia24.shop.db.entity.ProductEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository : CrudRepository<ProductEntity, String> {

    fun findBySku(sku: String): ProductEntity?

    // It actually took me some digging to find out how the CrudRepository works XDDD
    // Pretty neat abstraction that looked like magic until I found out that it
    // Compiles queries from the method name itself
    fun findAllBySkuIn(skus: Collection<String>): Iterable<ProductEntity>
}